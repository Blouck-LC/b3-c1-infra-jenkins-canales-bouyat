from src.calculatrice import Calculatrice

import unittest

class TestCalculatrice(unittest.TestCase):
    def setUp(self):
        #Test Carre et Racine Carre
        self.calc = Calculatrice(2,0)
        self.calc0 = Calculatrice(-5,0)
        self.calc1 = Calculatrice(6.2,0)
        self.calc2 = Calculatrice(0,0)
        #Test Puissance
        self.calc_P1 = Calculatrice (0,3)
        self.calc_P2 = Calculatrice (2,0)
        self.calc_P3 = Calculatrice (5,3)
        self.calc_P4 = Calculatrice (-2,4)
        self.calc_P5 = Calculatrice (-3,-6)
        self.calc_P6 = Calculatrice (4,-2)
        self.calc_P7 = Calculatrice (4.4,4)
        self.calc_P8 = Calculatrice (6,2.4)

    def test_carre(self):
        # On vérifie que le carré d'un nombre + , - , avirgule et 0
        self.assertEqual(self.calc.carre(), 4)
        self.assertEqual(self.calc0.carre(), 25)
        self.assertEqual(self.calc1.carre(), 38.440)
        self.assertEqual(self.calc2.carre(), 0)
        print("Test Carré Ok !")

    def test_racine_carre(self):
        # On vérifie que la racine carré d'un nombre + , - , avirgule et 0
        self.assertEqual(self.calc.racine_carre(), 1.414)
        self.assertEqual(self.calc0.racine_carre(),"Erreur, impossible de faire la racine carré d'un nombre négatif")
        self.assertEqual(self.calc1.racine_carre(), 2.49)
        self.assertEqual(self.calc2.racine_carre(), 0)
        print("Test Racine Carré Ok !")
    
    def test_puissance(self):
        # On Test la puissance d'un nombre + , - , avirgule et 0
        self.assertEqual(self.calc_P1.puissance(), 0)
        self.assertEqual(self.calc_P2.puissance(), 1)
        self.assertEqual(self.calc_P3.puissance(), 125)
        self.assertEqual(self.calc_P4.puissance(), 16)
        self.assertEqual(self.calc_P5.puissance(), 0.001)
        self.assertEqual(self.calc_P6.puissance(), 0.062)
        self.assertEqual(self.calc_P7.puissance(), 374.81)
        self.assertEqual(self.calc_P8.puissance(), 73.716)
        print("Test Puissance Ok !")

if __name__ == '__main__':
    unittest.main()         