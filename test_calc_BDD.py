from src.calculatrice import Calculatrice

import unittest

class TestCalculatrice(unittest.TestCase):
    def setUp(self):
        self.calc = Calculatrice(5, 2)
        self.calc0 = Calculatrice(5,0)
        self.calc1 = Calculatrice(-400,60)
        self.calc2 = Calculatrice(-45,-45)
        self.calc3 = Calculatrice(67.745,1.48)

    def test_addition(self):
        # On vérifie que l'addition de 5 et 2 donne bien 7
        self.assertEqual(self.calc.addition(), 7)
        self.assertEqual(self.calc0.addition(), 5)
        self.assertEqual(self.calc1.addition(), -340)
        self.assertEqual(self.calc2.addition(), -90)
        self.assertEqual(self.calc3.addition(), 69.225)
        print("Test Addition OK !")


    def test_soustraction(self):
        # On vérifie que la soustraction de 5 et 2 donne bien 3
        self.assertEqual(self.calc.soustraction(), 3)
        self.assertEqual(self.calc0.soustraction(), 5)
        self.assertEqual(self.calc1.soustraction(), -460)
        self.assertEqual(self.calc2.soustraction(), 0)
        self.assertEqual(self.calc3.soustraction(), 66.265)
        print("Test Soustraction OK !")

    def test_multiplication(self):
        # On vérifie que la multiplication de 5 et 2 donne bien 10
        self.assertEqual(self.calc.multiplication(), 10)
        self.assertEqual(self.calc0.multiplication(), 0)
        self.assertEqual(self.calc1.multiplication(), -24000)
        self.assertEqual(self.calc2.multiplication(), 2025)
        self.assertEqual(self.calc3.multiplication(), 100.263)
        print("Test Multiplication OK !")

    def test_division(self):
        # On vérifie que la division de 5 par 2 donne bien 2.5
        self.assertEqual(self.calc.division(), 2.5)
        self.assertEqual(self.calc1.division(), -6.667)
        self.assertEqual(self.calc2.division(), 1)
        self.assertEqual(self.calc3.division(), 45.774)
        print("Test Division OK !")

    def test_division_par_zero(self):
         #On vérifie que la division par 0 donne bien Erreur 
        self.assertEqual(self.calc0.division(),"Erreur , impossible de diviser par 0")
        print("Test Division par zero OK !")

    def test_pourcentage(self):
        # On vérifie que le pourcentage de 5 sur 2 donne bien 0.1
        self.assertEqual(self.calc.pourcentage(), 0.1)
        self.assertEqual(self.calc0.pourcentage(), 0)
        self.assertEqual(self.calc1.pourcentage(), -240)
        self.assertEqual(self.calc2.pourcentage() , 20.25)
        self.assertEqual(self.calc3.pourcentage() , 1.003)
        print("Test Pourcentage OK !")
   
if __name__ == '__main__':
    unittest.main() 