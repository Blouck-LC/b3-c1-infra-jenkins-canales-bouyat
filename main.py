from src.calculatrice import Calculatrice

while True:
    calculatrice = Calculatrice(0,0)

    print("\n-------------Info Calculatrice-------------\n")
    print("Liste des opérateurs : \n")
    print("1 = + | 2 = - | 3 = * | 4 = / | 5 = % | 6 = ² | 7 = Racine Carrée | 8 = Puissance d'un nombre x^y\n")



    nombre1 = float(input("Entrer le premier nombre : "))
    choix = int(input("Choix du nombre de l'opérateur : "))
    if choix > 0 & choix < 9 :
        calculatrice.entrer_nombre1(nombre1)
        if choix == 6 or choix == 7:
                if choix == 6:
                        resultat = calculatrice.carre()
                elif choix == 7:
                        resultat = calculatrice.racine_carre()
        else:
                nombre2 = float(input("Entrer le deuxieme nombre : "))
                calculatrice.entrer_nombre2(nombre2)

                if choix == 1:
                        resultat = calculatrice.addition()
                elif choix == 2:
                        resultat = calculatrice.soustraction()
                elif choix == 3:
                        resultat = calculatrice.multiplication()
                elif choix == 4:
                        resultat = calculatrice.division()
                elif choix == 5:
                        resultat = calculatrice.pourcentage()
                elif choix == 8:
                        resultat = calculatrice.puissance()
                else:
                        print("Opérateur incorrect.\n")

                print("\nLe résultat est : ", resultat)

    else:
        print("Erreur d'opérateur : choisir avec la liste au-dessus")
