# b3-dev-tu-canales-loucas

auteur : CANALES Loucas / BOUYAT Kyllian

# Projet 

 Développer une calculatrice scientifique
  
# Fonctionnalité 

●Opérations arithmétique (BDD) de base (addition, soustraction multiplication,  division, calcule d’un pourcentage)

●Partie scientifique (TDD) ○calcule du carré d’un nombre et de la racine carré○calcule de puissance d’un nombre

# Langaage utilisé 

Python

# Test Unitaire

unittest

# Utilisation Calculatrice 

1. Lancer la calculatrice.
2. Saisissez le premier nombre que vous souhaitez utiliser dans l'opération.
3. Sélectionnez l'opérateur que vous souhaitez utiliser (par exemple, 1 : + pour l'addition, 2 : - pour la soustraction, 3 : * pour la multiplication et 4 : / pour la division).
4. Saisissez le deuxième nombre que vous souhaitez utiliser dans l'opération (Sauf pour le 6 et le 7 donc Carré et Racine Carré ou seul le nombre 1 est demandé.
5. Le résultat de l'opération s'affiche (ou une ERREUR en cas de calcul impossible).

# Lancer les Tests unitaire 

1. Aller dans le fichier test_calc_BDD ou test_calc-TDD.
2. Faite Star Debugging (F5)
3. Les tests Unitaires vont être lancés
4. Affichage du résultat
