class Calculatrice:
    def __init__(self,nombre1,nombre2):
        self.nombre1 = nombre1
        self.nombre2 = nombre2
        self.resultat = 0

    def entrer_nombre1(self, nombre1):
        self.nombre1 = nombre1

    def entrer_nombre2(self, nombre2):
        self.nombre2 = nombre2

    def addition(self):
        self.resultat = self.nombre1 + self.nombre2
        return round(self.resultat,3)

    def soustraction(self):
        self.resultat = self.nombre1 - self.nombre2
        return round(self.resultat,3)

    def multiplication(self):
        self.resultat = self.nombre1 * self.nombre2
        return round(self.resultat,3)

    def division(self):
        if self.nombre1 == 0 or self.nombre2 == 0:
            self.resultat = ("Erreur , impossible de diviser par 0")
            return self.resultat
        else:
            self.resultat = self.nombre1 / self.nombre2
        return round (self.resultat,3)

    def pourcentage(self):
        self.resultat = (self.nombre1 / 100) * self.nombre2
        return round(self.resultat,3)

#------------ TEST TDD ----------------------------

    def carre(self):
        self.resultat = self.nombre1 ** 2
        return round (self.resultat , 3)

    def racine_carre(self):
        if self.nombre1 < 0:
            self.resultat = ("Erreur, impossible de faire la racine carré d'un nombre négatif")
            return self.resultat
        else:    
            self.resultat = self.nombre1 ** (1/2)
            return round (self.resultat, 3)
    
    def puissance(self):
        self.resultat = self.nombre1 ** self.nombre2
        return round (self.resultat, 3)